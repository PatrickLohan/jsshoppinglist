var button = document.getElementById("enter");
var input = document.getElementById("input");
var ul = document.querySelector("ul");

function inputLength() {
  return input.value.length;
}

function addNewItem() {
  var li = document.createElement("li");
  li.appendChild(document.createTextNode(input.value));
  ul.appendChild(li);
  input.value = "";
}

function addNewItemAfterClick(){
  if (inputLength() > 0){
    addNewItem();
  }
}

function addNewItemAfterEnter(event){
  if (inputLength() > 0 && event.keyCode === 13){
    addNewItem();
  }
}

button.addEventListener("click", addNewItemAfterClick);

input.addEventListener("keypress", addNewItemAfterEnter);
